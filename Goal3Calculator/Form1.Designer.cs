﻿namespace Goal3Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dot = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.equals = new System.Windows.Forms.Button();
            this.negate = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.one = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.mutiply = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.root = new System.Windows.Forms.Button();
            this.square = new System.Windows.Forms.Button();
            this.inverse = new System.Windows.Forms.Button();
            this.percent = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultLabel
            // 
            this.resultLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.Location = new System.Drawing.Point(4, 26);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(241, 79);
            this.resultLabel.TabIndex = 0;
            this.resultLabel.Text = "0";
            this.resultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.dot);
            this.panel1.Controls.Add(this.zero);
            this.panel1.Controls.Add(this.equals);
            this.panel1.Controls.Add(this.negate);
            this.panel1.Controls.Add(this.two);
            this.panel1.Controls.Add(this.three);
            this.panel1.Controls.Add(this.plus);
            this.panel1.Controls.Add(this.one);
            this.panel1.Controls.Add(this.five);
            this.panel1.Controls.Add(this.six);
            this.panel1.Controls.Add(this.minus);
            this.panel1.Controls.Add(this.four);
            this.panel1.Controls.Add(this.eight);
            this.panel1.Controls.Add(this.nine);
            this.panel1.Controls.Add(this.mutiply);
            this.panel1.Controls.Add(this.seven);
            this.panel1.Controls.Add(this.clear);
            this.panel1.Controls.Add(this.back);
            this.panel1.Controls.Add(this.divide);
            this.panel1.Controls.Add(this.start);
            this.panel1.Controls.Add(this.root);
            this.panel1.Controls.Add(this.square);
            this.panel1.Controls.Add(this.inverse);
            this.panel1.Controls.Add(this.percent);
            this.panel1.Location = new System.Drawing.Point(2, 146);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(243, 318);
            this.panel1.TabIndex = 1;
            // 
            // dot
            // 
            this.dot.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.dot.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dot.Location = new System.Drawing.Point(131, 262);
            this.dot.Name = "dot";
            this.dot.Size = new System.Drawing.Size(46, 43);
            this.dot.TabIndex = 30;
            this.dot.Text = ".";
            this.dot.UseVisualStyleBackColor = false;
            this.dot.Click += new System.EventHandler(this.dot_Click);
            // 
            // zero
            // 
            this.zero.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.zero.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zero.Location = new System.Drawing.Point(71, 259);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(46, 43);
            this.zero.TabIndex = 29;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = false;
            this.zero.Click += new System.EventHandler(this.zero_Click);
            // 
            // equals
            // 
            this.equals.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.equals.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equals.Location = new System.Drawing.Point(191, 259);
            this.equals.Name = "equals";
            this.equals.Size = new System.Drawing.Size(46, 43);
            this.equals.TabIndex = 28;
            this.equals.Text = "=";
            this.equals.UseVisualStyleBackColor = false;
            this.equals.Click += new System.EventHandler(this.equals_Click);
            // 
            // negate
            // 
            this.negate.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.negate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.negate.Location = new System.Drawing.Point(10, 259);
            this.negate.Name = "negate";
            this.negate.Size = new System.Drawing.Size(46, 43);
            this.negate.TabIndex = 27;
            this.negate.Text = "±";
            this.negate.UseVisualStyleBackColor = false;
            this.negate.Click += new System.EventHandler(this.negate_Click);
            // 
            // two
            // 
            this.two.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.two.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.two.Location = new System.Drawing.Point(71, 210);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(46, 43);
            this.two.TabIndex = 26;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = false;
            this.two.Click += new System.EventHandler(this.two_Click);
            // 
            // three
            // 
            this.three.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.three.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.three.Location = new System.Drawing.Point(131, 210);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(46, 43);
            this.three.TabIndex = 25;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = false;
            this.three.Click += new System.EventHandler(this.three_Click);
            // 
            // plus
            // 
            this.plus.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plus.Location = new System.Drawing.Point(191, 210);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(46, 43);
            this.plus.TabIndex = 24;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = false;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // one
            // 
            this.one.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.one.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.one.Location = new System.Drawing.Point(10, 210);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(46, 43);
            this.one.TabIndex = 23;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = false;
            this.one.Click += new System.EventHandler(this.one_Click);
            // 
            // five
            // 
            this.five.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.five.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.five.Location = new System.Drawing.Point(70, 160);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(46, 43);
            this.five.TabIndex = 22;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = false;
            this.five.Click += new System.EventHandler(this.five_Click);
            // 
            // six
            // 
            this.six.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.six.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.six.Location = new System.Drawing.Point(130, 160);
            this.six.Name = "six";
            this.six.Size = new System.Drawing.Size(46, 43);
            this.six.TabIndex = 21;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = false;
            this.six.Click += new System.EventHandler(this.six_Click);
            // 
            // minus
            // 
            this.minus.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minus.Location = new System.Drawing.Point(190, 160);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(46, 43);
            this.minus.TabIndex = 20;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = false;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // four
            // 
            this.four.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.four.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.four.Location = new System.Drawing.Point(9, 160);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(46, 43);
            this.four.TabIndex = 19;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = false;
            this.four.Click += new System.EventHandler(this.four_Click);
            // 
            // eight
            // 
            this.eight.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.eight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eight.Location = new System.Drawing.Point(71, 111);
            this.eight.Name = "eight";
            this.eight.Size = new System.Drawing.Size(46, 43);
            this.eight.TabIndex = 18;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = false;
            this.eight.Click += new System.EventHandler(this.eight_Click);
            // 
            // nine
            // 
            this.nine.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.nine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nine.Location = new System.Drawing.Point(131, 111);
            this.nine.Name = "nine";
            this.nine.Size = new System.Drawing.Size(46, 43);
            this.nine.TabIndex = 17;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = false;
            this.nine.Click += new System.EventHandler(this.nine_Click);
            // 
            // mutiply
            // 
            this.mutiply.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.mutiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mutiply.Location = new System.Drawing.Point(191, 111);
            this.mutiply.Name = "mutiply";
            this.mutiply.Size = new System.Drawing.Size(46, 43);
            this.mutiply.TabIndex = 16;
            this.mutiply.Text = "x";
            this.mutiply.UseVisualStyleBackColor = false;
            this.mutiply.Click += new System.EventHandler(this.mutiply_Click);
            // 
            // seven
            // 
            this.seven.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.seven.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seven.Location = new System.Drawing.Point(10, 111);
            this.seven.Name = "seven";
            this.seven.Size = new System.Drawing.Size(46, 43);
            this.seven.TabIndex = 15;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = false;
            this.seven.Click += new System.EventHandler(this.seven_Click);
            // 
            // clear
            // 
            this.clear.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear.Location = new System.Drawing.Point(71, 62);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(46, 43);
            this.clear.TabIndex = 14;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = false;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.back.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.Location = new System.Drawing.Point(130, 62);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(46, 43);
            this.back.TabIndex = 13;
            this.back.Text = "back";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // divide
            // 
            this.divide.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.divide.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.divide.Location = new System.Drawing.Point(191, 62);
            this.divide.Name = "divide";
            this.divide.Size = new System.Drawing.Size(46, 43);
            this.divide.TabIndex = 12;
            this.divide.Text = "÷";
            this.divide.UseVisualStyleBackColor = false;
            this.divide.Click += new System.EventHandler(this.divide_Click);
            // 
            // start
            // 
            this.start.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.start.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.start.Location = new System.Drawing.Point(10, 62);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(46, 43);
            this.start.TabIndex = 11;
            this.start.Text = "CE";
            this.start.UseVisualStyleBackColor = false;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // root
            // 
            this.root.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.root.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.root.Location = new System.Drawing.Point(70, 12);
            this.root.Name = "root";
            this.root.Size = new System.Drawing.Size(46, 43);
            this.root.TabIndex = 10;
            this.root.Text = "√";
            this.root.UseVisualStyleBackColor = false;
            this.root.Click += new System.EventHandler(this.root_Click);
            // 
            // square
            // 
            this.square.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.square.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square.Location = new System.Drawing.Point(130, 12);
            this.square.Name = "square";
            this.square.Size = new System.Drawing.Size(46, 43);
            this.square.TabIndex = 9;
            this.square.Text = "x^2";
            this.square.UseVisualStyleBackColor = false;
            this.square.Click += new System.EventHandler(this.square_Click);
            // 
            // inverse
            // 
            this.inverse.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.inverse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inverse.Location = new System.Drawing.Point(190, 12);
            this.inverse.Name = "inverse";
            this.inverse.Size = new System.Drawing.Size(46, 43);
            this.inverse.TabIndex = 8;
            this.inverse.Text = "1/x";
            this.inverse.UseVisualStyleBackColor = false;
            this.inverse.Click += new System.EventHandler(this.button8_Click);
            // 
            // percent
            // 
            this.percent.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.percent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percent.Location = new System.Drawing.Point(9, 12);
            this.percent.Name = "percent";
            this.percent.Size = new System.Drawing.Size(46, 43);
            this.percent.TabIndex = 0;
            this.percent.Text = "%";
            this.percent.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 463);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.resultLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button dot;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button equals;
        private System.Windows.Forms.Button negate;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button six;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button mutiply;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button divide;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button root;
        private System.Windows.Forms.Button square;
        private System.Windows.Forms.Button inverse;
        private System.Windows.Forms.Button percent;
    }
}

