﻿using System;
using System.Windows.Forms;

namespace Goal3Calculator
{
    public partial class Form1 : Form
    {
        double currOperand = 0;
        string currOperator = "";
        bool clearResult = true;
        bool noOperand = true;
        bool equalsPressedLast = false;

        public Form1()
        {
            InitializeComponent();
        }

       

        private void one_Click(object sender, EventArgs e)
        {
            if (clearResult)
            {
                resultLabel.Text = "1";
            }
            else
            {
                resultLabel.Text = resultLabel.Text+"1";
            }

            clearResult = false;
        }

        private void two_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "2";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "2";
            }

            clearResult = false;
        }

        private void three_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "3";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "3";
            }

            clearResult = false;
        }

        private void zero_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "0";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "0";
            }

            clearResult = false;
        }

        private void four_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "4";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "4";
            }

            clearResult = false;
        }

        private void five_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "5";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "5";
            }

            clearResult = false;
        }

        private void six_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "6";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "6";
            }

            clearResult = false;
        }

        private void seven_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "7";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "7";
            }

            clearResult = false;
        }

        private void eight_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "8";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "8";
            }

            clearResult = false;
        }

        private void nine_Click(object sender, EventArgs e)
        {
            if (clearResult || resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = "9";
            }
            else
            {
                resultLabel.Text = resultLabel.Text + "9";
            }

            clearResult = false;
        }

        private void back_Click(object sender, EventArgs e)
        {
            if(resultLabel.Text.Length == 1)
            {
                resultLabel.Text = "0";
            }
            else if (!resultLabel.Text.Equals("0"))
            {
                resultLabel.Text = resultLabel.Text.Substring(0, resultLabel.Text.Length - 1);
            }

            clearResult = false;
        }

        private void dot_Click(object sender, EventArgs e)
        {
            if (resultLabel.Text.IndexOf(".") == -1)
            {
                if (clearResult || resultLabel.Text.Equals("0"))
                {
                    resultLabel.Text = "0.";
                }
                else
                {
                    resultLabel.Text = resultLabel.Text + ".";
                }
            }

            clearResult = false;
        }

        private void negate_Click(object sender, EventArgs e)
        {
            if (!resultLabel.Text.Equals("0"))
            {
                if (resultLabel.Text[0] == '-')
                {
                    resultLabel.Text = resultLabel.Text.Substring(1);
                }
                else
                {
                    resultLabel.Text = "-" + resultLabel.Text;
                }
            }
            clearResult = false;
        }

        private void start_Click(object sender, EventArgs e)
        {
            resultLabel.Text = "0";
            currOperand = 0;
            currOperator = "";
            noOperand = true;
        }

        private void clear_Click(object sender, EventArgs e)
        {
            resultLabel.Text = "0";
        }

        private void divide_Click(object sender, EventArgs e)
        {
            if ((currOperand != 0 || !currOperator.Equals("")) && !equalsPressedLast)
            {
                double ans = calculate();
                resultLabel.Text = ans.ToString();
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "/";
            }
            else
            {
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "/";
            }
            clearResult = true;
            noOperand = false;
            equalsPressedLast = false;
        }


        private void mutiply_Click(object sender, EventArgs e)
        {
            if ((currOperand != 0 || !currOperator.Equals("")) && !equalsPressedLast)
            {
                double ans = calculate();
                resultLabel.Text = ans.ToString();
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "*";
            }
            else
            {
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "*";
            }
            clearResult = true;
            noOperand = false;
            equalsPressedLast = false;
        }

        private void minus_Click(object sender, EventArgs e)
        {
            if ((currOperand != 0 || !currOperator.Equals("")) && !equalsPressedLast)
            {
                double ans = calculate();
                resultLabel.Text = ans.ToString();
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "-";
            }
            else
            {
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "-";
            }
            clearResult = true;
            noOperand = false;
            equalsPressedLast = false;
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if ((currOperand != 0 || !currOperator.Equals("")) && !equalsPressedLast)
            {
                double ans = calculate();
                resultLabel.Text = ans.ToString();
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "+";
            }
            else
            {
                currOperand = double.Parse(resultLabel.Text);
                currOperator = "+";
            }
            clearResult = true;
            noOperand = false;
            equalsPressedLast = false;
        }

        private void equals_Click(object sender, EventArgs e)
        {
            resultLabel.Text = calculate().ToString();
            //noOperand = true;
            clearResult = true;
            //currOperator = "";
            equalsPressedLast = true;

        }


        private void root_Click(object sender, EventArgs e)
        {
            resultLabel.Text = Math.Pow(double.Parse(resultLabel.Text), 0.5).ToString();
            clearResult = true;
        }

        private void square_Click(object sender, EventArgs e)
        {
            resultLabel.Text = Math.Pow(double.Parse(resultLabel.Text), 2).ToString();
            clearResult = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            resultLabel.Text = (1 /double.Parse(resultLabel.Text)).ToString();
            clearResult = true;
        }


        double calculate()
        {
            if (noOperand)
            {
                return double.Parse(resultLabel.Text);
            }
            else
            {
                double operand2 = double.Parse(resultLabel.Text);
                if (currOperator.Equals("/"))
                {
                    return currOperand / operand2;
                }
                else if (currOperator.Equals("*"))
                {
                    return currOperand * operand2;
                }
                else if (currOperator.Equals("-"))
                {
                    return currOperand - operand2;
                }
                else if (currOperator.Equals("+"))
                {
                    return currOperand + operand2;
                }
            }


            return 0;
        }

    }
}
